package com.everis.escuela.repository;

import com.everis.escuela.dto.FindByProductIdDto;
import com.everis.escuela.entity.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StockRepository extends JpaRepository<Stock, Integer> {
/*
    @Query(value = "SELECT sum(s.quantity)  FROM Stock s " +
            "where s.productId = :productId group by s.productId")
    Integer findByProductIdAndSum(Integer productId);*/

    List<Stock> findByProductId(Integer productId);
}
