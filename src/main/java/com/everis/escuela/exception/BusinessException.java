package com.everis.escuela.exception;

import lombok.Getter;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@Getter
public class BusinessException extends Exception{
    private static final long serialVersionUID = 1L;

    private String message;
    private String dateTime;

    public BusinessException(String message){
        this.message = message;
        this.dateTime = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
                .withZone(ZoneId.systemDefault())
                .format(Instant.now());
    }
}
