package com.everis.escuela.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "Stock")
public class Stock {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Integer id;
    @Column
    private Integer productId;
    @Column
    private Integer wareHouseId;
    @Column
    private Integer quantity;
}
