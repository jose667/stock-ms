package com.everis.escuela.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FindByProductIdDto {
    private Integer productId;
    private Integer total;
}
