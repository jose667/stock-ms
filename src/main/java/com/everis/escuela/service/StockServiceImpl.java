package com.everis.escuela.service;

import com.everis.escuela.entity.Stock;
import com.everis.escuela.exception.BusinessException;
import com.everis.escuela.exception.ResourceNotFoundException;
import com.everis.escuela.repository.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class StockServiceImpl implements StockService {

    @Autowired
    StockRepository stockRepository;

    @Override
    public Integer findByProductId(Integer productId) throws ResourceNotFoundException {
        List<Stock> stocks= stockRepository.findByProductId(productId);
        int total = 0;
        if (CollectionUtils.isEmpty(stocks)) {
            throw new ResourceNotFoundException();
        }
        for(Stock stock: stocks){
            total = total + stock.getQuantity();
        }

        return total;
    }

    @Override
    public List<Stock> save(List<Stock> stocks) throws BusinessException {
        for(Stock stock: stocks){
            if(stock == null || stock.getQuantity()==0){
                throw new BusinessException("Quantity of product cannot be 0");
            }
        }
        return stockRepository.saveAll(stocks);
    }
}
