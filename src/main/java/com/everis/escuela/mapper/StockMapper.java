package com.everis.escuela.mapper;

import com.everis.escuela.dto.FindByProductIdDto;
import com.everis.escuela.dto.SaveStockRequestDto;
import com.everis.escuela.dto.SaveStockResponseDto;
import com.everis.escuela.entity.Stock;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface StockMapper {

    StockMapper INSTANCE = Mappers.getMapper(StockMapper.class);

    Stock toEntity(SaveStockRequestDto saveStockRequestDto);

    SaveStockResponseDto toSaveStockResponseDto(Stock stock);

    FindByProductIdDto toFindByProductIdDto(Integer total, Integer productId);

    List<Stock> toEntity(List<SaveStockRequestDto> saveStockRequestDtos);

    List<SaveStockResponseDto> toSaveStockResponseDto(List<Stock> stocks);

}
