package com.everis.escuela.service;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import com.everis.escuela.entity.Stock;
import com.everis.escuela.exception.BusinessException;
import com.everis.escuela.exception.ResourceNotFoundException;
import com.everis.escuela.repository.StockRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class TestStockServiceShould {
    @InjectMocks
    private StockServiceImpl service;

    @Mock
    private StockRepository repository;

    @Test
    public void testFindByProductId() throws ResourceNotFoundException {
        //when(repository.findByProductIdAndSum(2)).thenReturn(19);
        Stock stock = new Stock();
        stock.setQuantity(4);
        List<Stock> stocks = new ArrayList<>();
        stocks.add(stock);
        when(repository.findByProductId(2)).thenReturn(stocks);

        Integer result = service.findByProductId(2);

        assertNotNull(result);
    }

    @Test
    public void testSave() throws BusinessException {
        Stock stock = new Stock();
        stock.setId(2);
        stock.setProductId(2);
        stock.setWareHouseId(2);
        stock.setQuantity(12);
        List<Stock> stocks = new ArrayList<>();
        stocks.add(stock);
        when(repository.saveAll(stocks)).thenReturn(stocks);

        List<Stock> result = service.save(stocks);

        assertNotNull(result);
        assertNotNull(result.get(0).getId());
        assertNotNull(result.get(0).getProductId());
        assertNotNull(result.get(0).getWareHouseId());
        assertNotNull(result.get(0).getQuantity());
    }
}
