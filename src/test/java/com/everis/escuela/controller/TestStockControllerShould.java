package com.everis.escuela.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.everis.escuela.dto.FindByProductIdDto;
import com.everis.escuela.dto.SaveStockRequestDto;
import com.everis.escuela.dto.SaveStockResponseDto;
import com.everis.escuela.entity.Stock;
import com.everis.escuela.exception.BusinessException;
import com.everis.escuela.exception.ResourceNotFoundException;
import com.everis.escuela.mapper.StockMapper;
import com.everis.escuela.service.StockService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class TestStockControllerShould {
    @InjectMocks
    private StockController controller;

    @Mock
    private StockService service;

    @Test
    public void testFindByProductId() throws ResourceNotFoundException {
        FindByProductIdDto findByProductIdDto = new FindByProductIdDto();
        findByProductIdDto.setProductId(2);
        findByProductIdDto.setTotal(14);

        when(service.findByProductId(2)).thenReturn(14);

        FindByProductIdDto result = controller.findByProductId(2);

        assertNotNull(result);
        assertEquals(2, result.getProductId());
        assertEquals(14, result.getTotal());
    }

    @Test
    public void testSave() throws BusinessException {
        SaveStockRequestDto saveStockRequestDto = new SaveStockRequestDto();
        saveStockRequestDto.setProductId(5);
        saveStockRequestDto.setWareHouseId(1);
        saveStockRequestDto.setQuantity(14);

        List<SaveStockRequestDto> stockRequestDtos = new ArrayList<>();
        stockRequestDtos.add(saveStockRequestDto);


        Stock saved = new Stock();
        saved.setId(5);
        saved.setProductId(2);
        saved.setWareHouseId(1);
        saved.setQuantity(14);

        List<Stock> savedList = new ArrayList<>();
        savedList.add(saved);

        when(service.save(any(List.class))).thenReturn(savedList);

        List<SaveStockResponseDto> result = controller.save(stockRequestDtos);

        assertEquals(1, result.size());
        assertEquals(5, result.get(0).getId());
        assertEquals(1, result.get(0).getWareHouseId());
        assertEquals(2, result.get(0).getProductId());
        assertEquals(14, result.get(0).getQuantity());
    }
}
