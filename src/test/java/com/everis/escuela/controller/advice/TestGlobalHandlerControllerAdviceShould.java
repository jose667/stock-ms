package com.everis.escuela.controller.advice;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.everis.escuela.dto.ErrorDetailDto;
import com.everis.escuela.exception.BusinessException;
import com.everis.escuela.exception.ResourceNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

@RunWith(MockitoJUnitRunner.class)
public class TestGlobalHandlerControllerAdviceShould {

    @InjectMocks
    private GlobalHandlerControllerAdvice handler;

    @Test
    public void testResourceNotFoundException(){
        assertNotNull(handler.resourceNotFoundException(new ResourceNotFoundException()));
    }

    @Test
    public void testBusinessException(){
        BusinessException exception = new BusinessException("error");
        ResponseEntity<ErrorDetailDto> result = handler.businessException(exception);
        assertEquals(exception.getMessage(), result.getBody().getMessage());
        assertEquals(exception.getDateTime(), result.getBody().getDateTime());
    }
}
